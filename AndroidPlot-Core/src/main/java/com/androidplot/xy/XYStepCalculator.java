/*
 * Copyright 2012 AndroidPlot.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.androidplot.xy;

import android.graphics.RectF;
import com.androidplot.util.ValPixConverter;

/**
 * Calculates "stepping" values for a plot.  These values are most commonly used for
 * drawing grid lines on a graph.
 */
public class XYStepCalculator {


    /**
     * Convenience method - wraps other form of getStep().
     * @param plot
     * @param axisType
     * @param rect
     * @param minVal
     * @param maxVal
     * @return
     */
    public static XYStep getStep(XYPlot plot, XYAxisType axisType, RectF rect, Number minVal, Number maxVal, float originPix, float originPixVal) {
        XYStep step = null;
        switch(axisType) {
            case DOMAIN:
                step = getStep(plot.getDomainStepMode(), rect.width(), plot.getDomainStepValue(), minVal, maxVal, originPix, originPixVal);
                break;
            case RANGE:
                step = getStep(plot.getRangeStepMode(), rect.height(), plot.getRangeStepValue(), minVal, maxVal, originPix, originPixVal);
                break;
        }
        return step;
    }
    
    public static double getNiceNumber(double number) {
        final int exponent = (int) Math.floor(Math.log10( Math.abs(number)));
        double fraction  = Math.abs(number) * Math.pow(10, -exponent);
        int roundedFraction = 10;
        if (fraction < 1.5) {
            roundedFraction = 1;
        } else if (fraction < 3) {
            roundedFraction = 2;
        } else if (fraction < 7) {
            roundedFraction = 5;
        }
        return roundedFraction * Math.pow(10, exponent);
    }

    public static XYStep getStep(XYStepMode typeXY, float plotPixelSize, double stepValue, Number minVal, Number maxVal, float originPix, float originPixVal) {
        //XYStep step = new XYStep();
        double stepVal = 0;
        float stepPix = 0;
        final double minValue = minVal.doubleValue();
        final double maxValue = maxVal.doubleValue();
        final double length = maxValue - minValue;
        int minIndex = 0;
        int maxIndex = 0;
        double stepCount = stepValue;
        final float deltaUp =  plotPixelSize - originPix;
        final float deltaDown =  originPix;
        switch(typeXY) {
            case INCREMENT_BY_VAL:
                stepVal = stepValue;
                stepPix = (int)(stepValue/ ValPixConverter.valPerPix(minValue, maxValue, plotPixelSize));
                stepCount = plotPixelSize /stepPix;
                minIndex = (int) Math.ceil(- deltaDown / stepPix);
                maxIndex = (int) Math.ceil(- deltaUp / stepPix);
                break;
            case INCREMENT_BY_PIXELS:
                stepPix = new Double(stepValue).floatValue();
                stepCount = plotPixelSize /stepPix;
                minIndex = (int) Math.ceil(- deltaDown / stepPix);
                maxIndex = (int) Math.ceil(- deltaUp / stepPix);
                stepVal = ValPixConverter.valPerPix(minValue, maxValue, plotPixelSize)*stepPix;
                break;
            case SUBDIVIDE:
                stepCount = new Double(stepValue).floatValue();
                stepPix = (int) (plotPixelSize /(stepCount-1));
                minIndex = (int) Math.ceil(- deltaDown / stepPix);
                maxIndex = (int) Math.ceil(- deltaUp / stepPix);
                stepVal = ValPixConverter.valPerPix(minValue, maxValue, plotPixelSize)*stepPix;
                break;
            case AUTOMATIC:
                int numTicks = 5;
                stepVal = getNiceNumber(length / (numTicks - 1));
                stepPix = (int)(stepVal/ ValPixConverter.valPerPix(minValue, maxValue, plotPixelSize));
                minIndex = (int) Math.floor(minValue / stepVal);
                maxIndex = (int) Math.floor(maxValue / stepVal);
                originPixVal = (float) (minIndex * stepVal);
                originPix =  (int) ValPixConverter.valToPix(originPixVal, minValue, maxValue, plotPixelSize, true);
                maxIndex -= minIndex;
                minIndex = 0;
                stepCount = plotPixelSize /stepPix;
                break;
        }
        return new XYStep((float) stepCount, stepPix, stepVal, minIndex, maxIndex, originPix, originPixVal);
    }
}
